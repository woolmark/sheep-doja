import com.nttdocomo.ui.Display;
import com.nttdocomo.ui.IApplication;

public class Sheep extends IApplication {

  SheepCanvas canvas = null;

  public Sheep() {
  }

  public void start() {
    canvas = new SheepCanvas();
    Display.setCurrent(canvas);
    canvas.start();
  }

}
